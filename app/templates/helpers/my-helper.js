module.exports.funcion1 = function (str) {
	return 'Usada funcion1!';
};

module.exports.register = function(Handlebars, options) {
    Handlebars.registerHelper("for", function(from, to, incr, block) {
        var accum = '';
        for (var i = from; i < to; i += incr)
            accum += block.fn(i);
        return accum;
	});
	//Funcion para crear un bucle de repeticiones
    Handlebars.registerHelper("numberTitle", function(index) {
        var x = index + 2;
        if(x<10){
            x = "0"+x;
        }
        return x;
	});
	
	Handlebars.registerHelper('setIndex', function(value){
	    this.index = Number(value);
	});
};


module.exports.register = function (Handlebars, options)  {

	Handlebars.registerHelper('pf_commandButton', function ( argumentos ) {
		var id = argumentos.hash.id,
			texto = argumentos.hash.texto || "",
			icono = argumentos.hash.icono || "", // "ui-icon-disk"
			posicono = argumentos.hash.posicono || "icon-left", // "icon-left", "icon-right"
			cssClass = argumentos.hash.cssClass || "",
			onclick = argumentos.hash.onclick || "",
			typeButton = argumentos.hash.typeButton || "button";

		html = "<button id=\"" + id + "\" name=\"" + id + "\" class=\"ui-button ui-widget ui-state-default ui-corner-all " + cssClass;

		if ( texto === "" ) {
			html = html + " ui-button-icon-only \"";
		} else {
			if ( icono === "" ) {
				html = html + " ui-button-text-only \"";
			} else {
				html = html + " ui-button-text-" + posicono + " \"";
			}
		}

		if(onclick !== ""){
			html = html + " onclick=\"" + onclick + "\" ";
		}

		html = html + " type=\"" + typeButton + "\">\n";

		if( icono !== ""){
			html = html + "<span class=\"ui-button-" + posicono + " ui-icon ui-c " + icono + "\"></span>\n";
		}

		html = html + "<span class=\"ui-button-text ui-c\">" + (texto || "ui-button") + "</span>\n" +
			"</button>\n";

		return new Handlebars.SafeString( html );
	});
};
