;(function () {
	"use strict";
	module.exports = function(grunt) {

		var APP = {
			rutaapp:            'app',
			rutadev:            'dev',
		};

		// Cargamos los paquetes
		require('load-grunt-tasks')(grunt);
		require('time-grunt')(grunt);

		require('load-grunt-config')(grunt, {

			configPath: process.cwd() + '/grunt',
			init: true,

			// Variables adicionales
			config: {
				APP: APP
			},

			//loadGruntTasks: false,
			loadGruntTasks: {
				pattern: 'assemble',
				config: require('./package.json'),
				scope: 'devDependencies'
			}
		});


		grunt.registerTask('default', [
			"clean:dev",
			"copy:dev",
			"compass:dev",
			"assemble",
			"connect",
			"watch"
		]);

		grunt.registerTask('dist', [
			"clean:dev",
			"copy:dev",
			"compass:dist",
			"assemble",
			"connect",
			"watch"
		]);

	};
}());
