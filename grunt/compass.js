module.exports =  {
	dev: { 	// Another target
		options: {
			sassDir: 'app/sass',
			cssDir: 'dev/css',
			//outputStyle: 'compressed',
			force: true,
			noLineComments: true
		}
	},
	dist: { 	// Another target
		options: {
			sassDir: 'app/sass',
			cssDir: 'dev/css',
			outputStyle: 'compressed',
			force: true,
			noLineComments: true
		}
	}
};
