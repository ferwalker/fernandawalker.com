module.exports = {
	compass: {
		files: ["app/sass/{,*/}*.scss"],
		tasks: ["compass:dev"]
	},
	assemble: {
		files: ['app/templates/**/*.hbs'],
		tasks: ['assemble']
	},
	copyImg: {
		files: ['app/img/**/*'],
		tasks: ['clean:copyImg','compass:dev','copy']
	},
	copyJs: {
		files: ['app/js/**/*'],
		tasks: ['copy']
	},
	copyFonts: {
		files: ['app/fonts/**/*'],
		tasks: ['copy']
	}
};
