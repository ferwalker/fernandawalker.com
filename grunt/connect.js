module.exports = {
  options: {
    port: 9211,
    hostname: '*',
    base: '.',
    keepalive: false,
    livereload: false,
    open: 'http://localhost:<%= connect.options.port %>/'
  },
  livereload: {
    options: {
      base: ['<%= APP.rutadev %>/']
    }
  }
};
