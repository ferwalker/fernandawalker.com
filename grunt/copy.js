module.exports = {
	dev: {
		files: [
			{// app/img > dev/img
				expand: true,
				cwd: 'app/img/',
				src: ['**/*'],
				dest: 'dev/img/'
			},
			{// app/js > dev/js
				expand: true,
				cwd: 'app/js/',
				src: ['**/*'],
				dest: 'dev/js/'
			},
			{// app/mail > dev/mail
				expand: true,
				cwd: 'app/mail/',
				src: ['**/*'],
				dest: 'dev/mail/'
			},
            {// app/fonts > dev/fonts
				expand: true,
				cwd: 'app/fonts/',
				src: ['**/*'],
				dest: 'dev/fonts/'
			}
		]
	}
};
