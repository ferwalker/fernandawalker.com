module.exports = {
	options: {
		prettify: {
			indent: 2
		},
		marked: {
			sanitize: false
		},
		production: true,
		flatten: true,
		layoutdir: 'app/templates/layouts',
		layout: 'main-layout.hbs',
		data: 'app/templates/data/*.json',
		partials: [
			'app/templates/elements/**/*.hbs'
		],
		helpers: ['app/templates/helpers/*']

	},
	pages: {
		files: [{
			expand: true,
			cwd: 'app/templates/pages',
			src: ['*.hbs'],
			dest: 'dev',
			ext: '.html'
		}]
	},
	blog: {
		files: [{
			expand: true,
			cwd: 'app/templates/pagesBlog',
			layout: 'blog-layout.hbs',
			src: ['*.hbs'],
			dest: 'dev',
			ext: '.html'
		}]
	}
};
